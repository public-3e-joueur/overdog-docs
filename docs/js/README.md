# JS

## Norme

Lorsque possible, si vous devez seulement manipuler des actions dans le DOM,
privilégiez l'utilisation de vanilla JS sans framework.

__La norme es6 doit être utilisée pour tout nouveau code.__

### Code splitting

La fonction splitChunks de Webpack est utilisée. Utilisez la fonction de _dynamic import_ lorsque possible.

Plus d'info à venir.

## Babel

Babel est utilisé pour s'assurer que notre code est fonctionnel pour
un éventail suffisant de navigateurs.

La configuration par défaut de Babel est utilisée. Il s'agit de :

`> 0.5%, last 2 versions, Firefox ESR, not dead.`

Cette configaration se trouve dans le fichier `package.json` sous `browserslist`
