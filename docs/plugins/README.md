---
title: Plugins et mises à jour
lang: fr-CA
---

# Plugins et mises à jour

:::warning
Toujours __préférer le terminal__ pour ajouter et supprimer un plugin (à ne pas confondre avec installation / désinstallation).
:::

Le processus d'ajout et d'installation se fait en deux étapes.

## Ajouter

On télécharge dans notre dossier `vendors` le plugin avec `composer require`.
Il est du même coup ajouté à la liste de dépendances du projet dans le fichier `composer.json`.

::: tip
C'est ce fichier qui dicte à `composer install` quoi télécharger.
:::

#### Exemple avec le plugin Retour de nystudio107(Andrew Welch).

Le nom exact pour composer de chaque plugin se trouve sur sa page du Craft Plugin Store.

``` sh
composer require nystudio107/craft-retour
```

## Installer

L'installation de plugin crée les tables dans la base de données et "active" le _plugin_ dans le panneau de contrôle.
Cette étape peut se faire avec le terminal, mais le nom exact du _plugin_ n'est pas accessible aussi facilement.
Allez donc dans le panneau de contrôle dans :

`Settings > Plugins` et cliquez à droite sur __Install__.

## Supprimer

::: danger
Attention, toujours __désinstaller le plugin AVANT de supprimer__ avec Composer remove.
:::

1. Allez dans `Settings > Plugins` et <b>Uninstall</b>.
2. Faire `composer remove nystudio107/craft-retour`

_Notez que nystudio107/craft-retour est utilisé en exemple. Modifiez selon votre plugin._


## Fichiers de _config_

Cette fonction permet d'utiliser un fichier PHP pour stocker les configurations d'un _plugin_.
La majorité des créateurs de _plugin_ ajoute un exemple de ce fichier dans le dossier de leur _plugin_.

Il faut ensuite le copier et le renommer avec le nom du _plugin_ dans le dossier `config`.

::: warning
__À noter :__ Il s'agit d'une bonne pratique pour s'assurer que tous les développeurs ont les mêmes paramètres d'un _plugin_.
Lorsque présent, une mention dans le panneau de contrôle vous indique que le fichier _override_ ces paramètres.

__Exemple :__ `This is being overridden by the createColorPalette setting in the config/nom-du-plugin.php file.`
:::


Cette façon de faire permet aussi de mettre des valeurs différentes aux paramètres selon l'environnement (dev, staging, production).
