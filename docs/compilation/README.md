---
title: Compilation et live reload
lang: fr-CA
---


# Compilation et live reload

## Développement

Pour commencer à développer, allez dans le dossier du projet et faites dans votre terminal :

```sh
yarn start
```


Cela va démarrer la surveillance de vos fichiers, fusionner votre scss et JS lors de chaque sauvegarde et créer un proxy via Browsersync.

::: tip
Pour afficher les changements avec _auto reload_, utilisez `http://localhost:3000` Cela va prendre dans votre fichier `.env` la variable `$SITE_URL`
:::

#### Pour arrêter

``` sh
CTRL + C
```


## Production et staging

Afin de demeurer rapide lors du développenent, toute l'optimisation du code se fait lors du build en vue d'un déploiement en ligne.

::: warning
Avant de déployer en ligne, que ce soit en mode Staging ou Production, toujours faire :
:::

```sh
yarn build
```

:raised_back_of_hand: Attention : Votre serveur local doit etre actif lors du build.

### Que fait le _build_ :
1. Ajoute un hash devant les fichiers compilés pour le _cache busting_.
1. Minifie et crée les _chunks_ du code du projet, en séparant les vendors dans un fichier distinct.
2. Compile le JS selon la config Babel.
3. Génère un _Critical CSS_ pour la page d'accueil, la variable `CRITICALCSS` dans le fichier `.env` doit être à `true`.
4. Purge le CSS non-utilisé dans le dossier templates avec purgeCSS.
4. Supprime tout le contenu du dossier web/dist avant d'y mettre les nouveaux fichiers compilés.


## Paquets Node

#### Avec le terminal, à la racine de votre projet :


Ajouter un paquet pour le projet (Bootstrap, Flickity, etc.) :

``` sh
yarn add [package]
```

Ajouter un paquet pour le développement (Webpack, etc.) :

``` sh
yarn add [package] -D
```

Supprimer un paquet :

``` sh
yarn remove [package]
```

[Voir la documentation officielle de Yarn](https://yarnpkg.com/en/packages)
