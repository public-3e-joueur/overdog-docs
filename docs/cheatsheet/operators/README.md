---
title: Twig
lang: fr-CA
---

# Twig ternary operators 

## Operators


if `foo` echo `yes` else echo `no`

``` twig
{{ foo ? 'yes' : 'no' }}
```

***

If `foo` echo it, else echo `no`:

``` twig
{{ foo ?: 'no' }}
```
or

``` twig
{{ foo ? foo : 'no' }}
```

***

If `foo` echo `yes` else echo nothing:

``` twig
{{ foo ? 'yes' }}
```
or

``` twig
{{ foo ? 'yes' : '' }}
```

***

Returns the value of `foo` if it is __defined__ and __not null__, `no` otherwise:

``` twig
{{ foo ?? 'no' }}
```

***

## Default

Returns the value of `foo` if it is __defined__(empty values also count), `no` otherwise:

``` twig
{{ foo|default('no') }}
```
