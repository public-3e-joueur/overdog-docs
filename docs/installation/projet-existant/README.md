# Projet existant

Si vous souhaitez installer un projet existant, les étapes changent un peu. Voici la marche à suivre.

## Cloner de Gitlab

La première étape est de cloner sur votre ordinateur / serveur local le projet existant de son répertoire Gitlab.
Vous pouvez le faire avec votre logiciel habituel ou avec le terminal.

## Dépendances
Vous devez par la suite télécharger les dépendances du projet. Allez à la racine du projet avec votre terminal et faire :

### PHP
Téléchargez les paquets PHP du fichier `composer.json`

``` sh
composer install
```

### Node.js
Téléchargez les paquets Node.js nécessaires au développement et à la compilation du fichier `package.json`

``` sh
yarn install
```

## Base de données

### Faites un _dump_ de la base de données du projet live (production).

#### Façon #1 : Via le panneau de contrôle du site live

1. Allez dans l'onglet __Utilities__ du menu latéral et cliquez sur `Database Backup`.
2. Assurez-vous que le _checkbox_ `Download backup` est coché et cliquez sur le bouton.

#### Façon #2 : Via SSH

Si vous avez une connexion via SSH avec l'hébergeur, vous pouvez vous connecter à la base de données et faire un dump.


## Serveur local

### Serveur et base de données
1. Créez un nouveau Host sur votre serveur local.
2. Faites pointer ce host sur le `dossier web` de votre projet.
3. Créez une base de données MySQL. Vous pouvez utiliser l'utilisateur et mot de passe `root / root` comme ceci ne sera jamais déployé. Importez les tables de la base de données du projet _dumpée_ précédemment.


::: tip
Si vous utilisez Mamp, assurez-vous que _Allow network access to MySQL_ est coché dans l'onglet MySQL
:::

## Fichier .env
1. __Dupliquez__ le fichier `.env.example` à la racine du projet et renommez-le .env.
2. Inscrivez le URL de développement de votre host (sans _trailing slash_) et les infos de la base de données précédemment créée.
2. Allez dans `LastPass` chercher l'item `[ENV]` du projet et copiez-le en adaptant si nécessaire.
3. Ce fichier devrait contenir toutes les variables du projet.

[Plus de détails ici](/configuration/#fichier-env-lastpass)
