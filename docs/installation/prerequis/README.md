# Prérequis

### Pour l'installation

- [Composer installé sur votre ordinateur / serveur de développement](https://getcomposer.org)
- [PHP installé sur votre ordinateur / serveur de développement]( https://www.php.net/manual/fr/install.php)


### Pour les _builds_

- [Node.js installé sur votre ordinateur / serveur de développement](https://nodejs.org/en/)
- Le mode _development_ doit être activé, il s'agit du mode par défaut.


### Nouveau projet : Pour les _assets_ client <Badge text="v1.1+"/>

- Un `IAM user` client créé avec _programmatic access_. Demandez au responsable de vous le créer.
- __Vous devez avoir ces informations avant l'installation.__ Vous en aurez besoin dans votre fichier `.env`
- Responsables 3e joueur : Ian Reid Langevin, Sylvain Larek ou Mélissa Doyon

[En savoir plus](/configuration/#gestion-des-assets)
