---
title: Nouveau projet
lang: fr-CA
meta:
  - author: Ian Reid Langevin
---

# Nouveau projet

## Dossier de projet

1. Créez un dossier vide pour votre projet à l'endroit où vous mettez vos projets web.
2. Nommez ce dossier selon votre projet.

::: danger
_Composer_ fonctionne uniquement si le __dossier est vide__, donc ne pas ajouter `Git` à cette étape.
::::

## Téléchargement

### Overdog, dépendances PHP et scripts post-create du projet

Allez à la racine du projet avec votre terminal et faire :

``` sh
composer create-project 3ejoueur/overdog .
```
::: tip
Le point à la fin est important pour éviter de créer un sous-dossier.
::::

### Paquets Node.js pour le développement et la compilation

Allez à la racine du projet avec votre terminal et faire :

``` sh
yarn install
```


## Serveur local

### Serveur et base de données
1. Créez un nouveau Host sur votre serveur local.
2. Créez une base de données MySQL. Vous pouvez utiliser l'utilisateur et mot de passe `root / root` comme ceci ne sera jamais déployé.
3. Faites pointer ce host sur le `dossier web` de votre projet.

::: tip
Si vous utilisez __Mamp__, assurez-vous que _Allow network access to MySQL_ est coché dans l'onglet MySQL
:::

## Fichier .env
Dans le fichier `.env` à la racine du projet, allez remplir :

### Url de développement et base de données

- `SITE_URL` > le URL de développement de votre host (sans _trailing slash_)
- `DB_DRIVER` > mysql
- `DB_SERVER` > localhost ou 127.0.0.1 (exemple)
- `DB_PORT` > souvent 3306 ou 8889
- `DB_DATABASE` > nom de le base de données créée
- `DB_USER`> utilisateur (possiblement _root_)
- `DB_PASSWORD` > mot de passe (possiblement _root_)
- `DB_SCHEMA` > public
- `DB_TABLE_PREFIX` > laissez vide
- `SECURITY_KEY` > laissez vide, Craft va la générer lors de l'installation
- `APP_ID` > laissez vide

### Informations sur les volumes de stockage

::: warning
__Demandez au responsable de vous créer votre user client pour les assets.__
Les renseignements suivants vous seront fournis.
:::

- `S3_BUCKET_DOCS` > Nom du bucket des documents
- `S3_BUCKET_IMAGES` > Nom du bucket des images
- `S3_BUCKET_REGION` > ca-central-1
- `S3_BUCKET_SUBFOLDER` > Nom du client _url friendly_ en minuscule - sera dans le url des CDN.
- `S3_DOCS_URL` > Url du Cloudfront des documents
- `S3_DOCS_DIST_ID` > Id du Cloudfront pour les documents
- `S3_IMAGES_URL` > Url imgix des images
- `S3_USER_ID`  > User access Key ID
- `S3_USER_SECRET` > Secret Access Key

::: danger
Les informations des volumes de stockage sont disponibles dans __LastPass__. Ne JAMAIS conserver cela dans une note sur votre ordinateur. Toujours avoir un mot de passe sur votre ordinateur.
::::


## Installez Craft CMS

1. Dans votre dossier de projet, faire dans le terminal :

``` sh
php craft setup/welcome
```
> Cela va générer une `SECURITY_KEY` dans votre fichier `.env` et l'application ID. Cette clé sera la même pour tous, donc à ajouter dans LastPass. Plus d'info dans la section fichier .env

2. Suivre le processus d'installation et inscrire les renseignements de votre base de données, host, etc.

::: tip
__Pour le NOM DU SITE, le SITE URL et la LANGUE, faites seulement _Enter_. Votre installation va se synchroniser avec les paramètres de Overdog par la suite.__
::::


## Ajoutez Git

1. Créez votre projet sur Gitlab (demandez aux responsables si vous ne pouvez pas).
::: warning
__Ne cochez pas__ `Initialize repository with a readme`, le garder complètement vide.
:::
2. Ajouter votre dossier de projet précédemment créé dans votre logiciel ou terminal pour ajouter Git (la méthode diffère un peu selon l'outil utilisé).
3. Nommez ou renommez votre branche locale `develop`.
4. Ajouter le remote (Gitlab), idéalement via SSH, de votre projet créé chez Gitlab.
5. Faire un inital commit de develop et envoyer vers une nouvelle branche develop.
