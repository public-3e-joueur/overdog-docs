---
title: Configuration
lang: fr-CA
---

# Structure des dossiers

## Dossier templates

Nous utilisons le principe __"Convention is better than configuration".__


Chaque fichier de template se nomme `index.twig`. Nous créons un chemin de dossiers vers ce fichier selon son _section type_
à partir du dossier `templates/_sections`.

### Structure générale

``` md
TEMPLATES
  ├── _base
  │   ├── critical (critical CSS qui sera ajouté inline dans le head)
  │   └── hash (hash créés par Webpack vers les fichiers CSS et JS)
  ├── _macros
  ├── _navigation (votre navbar et menus)
  ├── _sections
  │   ├── single
  │   │   ├── home (exemple pour un single singleHome )
  │   │   │   └── index.twig
  │   │   └── news (exemple pour un single singleNews, la page d'archive )
  │   │       └── index.twig
  │   ├── channel
  │   │   ├── events (exemple pour un channel channelEvents, la page de chaque entrée )
  │   │   │   └── index.twig
  │   │   └── news (exemple pour un channel channelNews, la page de chaque entrée )
  │   │       └── index.twig
  │   ├── structure (même principe pour vos structures)
  │   ├── category (même principe pour vos catégories, si elles ont une page template)
  ├── _shared
  │   └── pagination.twig (exemple)
  ├── 404.twig
  ├── sitemap.xlm.twig (sitemap, modifiez selon votre contenu)
  ├── human.txt
  └── robot.txt
```

::: tip
Le _underscore_ devant les dossiers empêche d'accéder à ces dossiers à partir du web.
:::

### Dossier _sections

![nomenclature sections](./img/structure-templates.jpg)


#### Multiple entry types

![nomenclature sections](./img/multiple-entry-types.jpg)


### Handle vs Dossiers

La structure des dossiers est créée selon une combinaison du nom du handle

![nomenclature sections](./img/nomenclature-sections.jpg)

#### Multiple entry types

![nomenclature sections](./img/nomenclature-entry-types.jpg)

### Category Groups

![nomenclature category group](./img/nomenclature-category-group.jpg)


### Dossiers SRC et config

``` md
SRC
  ├── ejs (templates utilisés par Webpack pour le hash des fichiers)
  ├── js (nos fichiers JS)
  ├── sass (nos fichiers SCSS)
  └── svg (svg qui seront optimisés et ajoutés dans web/img/svg-sprite.svg)
      └── temp-optimized (dossier temporaire avec les svg optimisés)
```

``` md
CONFIG
  ├── project (fichiers yaml de structure de l'admin, générés automatiquement)
  ├── redactor (customs configs pour l'éditeur Redactor)
  └── general.php (fichier de config du projet)
```


## URI et slugs

Lors de la création d'une section, que ce soit une structure, un channel ou un single, il vous sera demandé pour chaque langue :

| Entry URI Format | Template  |
| ------ | -----|
| {slug} | _sections/ma-structure/index |

###  Incluant un _path_

La balise `{slug}` dit d'utiliser le champ `slug` de l'entrée en question. Vous pouvez donc combiner, exemple :

``` twig
nouvelles/{slug}
```

###  Incluant de la logique _Twig_

Ce champ accepte de la logique _Twig_, par exemple, la création d'un _slug_ basé sur la valeur d'un champ `checkbox`.

Cette façon dynamique peut s'avérer intéressante pour utiliser une valeur d'un champ categories pour créer le slug de l'entrée :

``` twig
{exempleCatRelation.one().slug}/{slug}
```

::: tip
Essayez toutefois de garder cela le plus simple et _"hardcodé"_ possible, pour faciliter la maintenance future.
:::

### Uri spéciaux

Si vous avez des besoins plus spécifiques, pour une page de résultat de recherche sans "entrée" ou autre, vous pouvez utiliser des routes custom:

Idéalement, utilisez le fichier routes.php dans config plutôt que le panneau d'administration.

[Documentation officielle sur les _custom routes_](https://craftcms.com/docs/3.x/routing.html#dynamic-routes)
