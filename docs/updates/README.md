---
title: Mises à jour
lang: fr-CA
---

# Mises à jour

::: danger
Les mises à jour se font __seulement__ en local et via le terminal.
:::

::: warning
__Astuce :__ Vous pouvez faire dans votre terminal `php craft update` pour afficher toutes les mises à jour disponibles.
Décidez par la suite si vous souhaitez cibler un _plugin_ en particulier ou effectuer toutes les mises à jour.
:::

[Documentation officielle > Updating](https://craftcms.com/docs/3.x/updating.html#updating-from-the-control-panel)

## Craft CMS

### Mise à jour de tout (plugins et Craft CMS)

On vous demandera ensuite si vous souhaitez faire un backup de la base de donnée

``` sh
php craft update all
```

### Mise à jour de seulement Craft

``` sh
php craft update craft
```

## Plugins

Pour mettre à jour un seul plugin, vous pouvez faire `php craft update` __+__ _handle du plugin_ :

Exemple avec le plugin Retour :

``` sh
php craft update retour
```

## Version spécifique

Ajouter la version désirée, cela fonctionne pour Craft et les plugins. Exemple :

``` sh
php craft update craft:3.5.19
```

## Breaking changes

Avant de faire une mise à jour, vérifier le changelog et s'assurer que le client dispose d'une banque d'heures
permettant de mettre le temps nécessaire à cette mise à jour.

[Changelog officiel > Craft CMS](https://github.com/craftcms/cms/blob/develop/CHANGELOG.md)
