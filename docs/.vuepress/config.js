// .vuepress/config.js
module.exports = {
  title: 'Overdog documentation', // Title for the site. This will be displayed in the navbar.
  theme: '@vuepress/theme-default',
  dest: 'public',
  themeConfig: {
    lastUpdated: 'Date de modification', // string | boolean
    sidebar: {
      '/cheatsheet/': [
        {
          title: 'Ternary operators',   // required
          path: '/cheatsheet/operators/',      // optional, link of the title, which should be an absolute path and must exist
          sidebarDepth: 2,    // optional, defaults to 1
          collapsable: false,
        },
      ],
      '/changelog/': [
        {
          title: 'Versions',   // required
          path: '/changelog/',      // optional, link of the title, which should be an absolute path and must exist
          sidebarDepth: 2,    // optional, defaults to 1
          collapsable: false,
        },
      ],
      '/': [
       {
         title: 'Installation',   // required
         collapsable: false,
         sidebarDepth: 1,
         children: [  "installation/prerequis/", "installation/nouveau-projet/", "installation/projet-existant/", ]
       },
       {
         title: 'Configuration',   // required
         path: '/configuration/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Dossiers',   // required
         path: '/dossiers/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Nomenclature',   // required
         path: '/nomenclature/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Compilation',   // required
         path: '/compilation/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Templating',   // required
         path: '/templating/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Utilitaires',   // required
         path: '/utilitaires/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Plugins',   // required
         path: '/plugins/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Mises à jour',   // required
         path: '/updates/', // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Scss',   // required
         path: '/scss/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'JS',   // required
         path: '/js/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
       {
         title: 'Déploiement',   // required
         path: '/deploiement/',      // optional, link of the title, which should be an absolute path and must exist
         sidebarDepth: 2,    // optional, defaults to 1
         collapsable: false,
       },
      ],
    },
    smoothScroll: true,
    nav: [
      { text: 'Cheatsheet', link: '/cheatsheet/operators/' },
      { text: 'Srcset', link: 'https://public-3e-joueur.gitlab.io/overdog-srcset/', target:'_blank' },
      { text: 'Changelog', link: '/changelog/' },
      { text: 'Packagist', link: 'https://packagist.org/packages/3ejoueur/overdog', target:'_blank' }
    ]
  }
}
