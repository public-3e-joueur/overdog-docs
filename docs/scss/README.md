---
title: SCSS
lang: fr-CA
---

# SCSS

Overdog fournit par défaut une base de dossiers et fichiers SCSS. Cette base est _opinionated_ pour être utilisée avec [SailorCSS](https://www.sailorcss.com/), S[SASS](https://sass-lang.com/documentation) et la méthodologie [BEM](https://scalablecss.com/static/BEM-cheat-sheet-c5d485af76bc3ec0fd23140951861136.pdf/).


## Organisation des dossiers

![nomenclature sections](./img/structure-scss.jpg)


## Ajout de vos partials

Chaque sous-dossier contient un fichier `index.scss`. 
C'est dans ce fichier que vous ajoutez les partials de son dossier.

::: danger
Normalement, vous ne devriez pas avoir besoin d'importer des _partials_ dans le fichier `index.scss` principal (celui à la racine du dossier SASS).
::::

## Utilisation des _mixins_ et _placeholders_

Pour utiliser un _mixin_ ou un _placeholder_, vous devez importer son _partial_ avec la règle @use. __Notez que @use et @forward remplacent la règle @import qui est maintenant dépréciée__ en SASS. 

La structure par défaut simplifie ce processus, puisque vous pouvez simplement @use le fichier hub de `/abstracts`. Voyez les exemples suivants :

### Mixins et placeholders provenant de SailorCSS

Voici un exemple pour utiliser n'importe quel mixin ou placeholder de SailorCSS. Vous pouvez consulter la [documentation](https://www.sailorcss.com/installation/) de SailorCSS pour plus d'information. 


``` scss
 // @use sailorcss (once) at the top of your file and add a shorter namespace
 @use "sailorcss" as s;

// You will need the namespace before mixins (not before placeholders)
.example-class {
   @include s.fluid-font(sm, xl, 24px, 48px);
   @extend %row-center-bottom; // placeholder flexbox de sailor
}

.other-class {
   @include s.media-up(lg) {
      font-size: 48px;
   }
}
```


::: tip
__Vous n'avez pas besoin d'ajouter le namespace en avant d'un placeholder.__
::::


### Mixins et placeholders provenant de votre code

Pour utiliser vos mixins et placeholders, provenant probablement de partials dans le dossier _abstract_, vous devez faire ceci : 

``` scss
 // @use sailorcss (once) at the top of your file and add a shorter namespace
 @use "../asbtracts" as a;

// You will need the namespace before mixins (not before placeholders)
.example-class {
   @include a.mon-mixin()
}

.other-class {
   @extend %title-02; // exemple de vos placeholders
}
```