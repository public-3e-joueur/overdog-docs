---
title: Configuration
lang: fr-CA
---

# Configuration

## Panneau de contrôle

Craft nomme panneau de contrôle (control panel) la zone Admin du CMS.
Connectez-vous en ajoutant `/admin` à votre url de projet.
```
http://votresite.com/admin
```

### 1. Modifiez la langue de votre utilisateur

Pour simplifier le suivi de la documentation, cliquez sur votre avatar en haut à droite et sur `Preferences`. Choisissez English à Language.
Dans le bas, cochez `Show the debug toolbar on the front end` pour afficher la barre de `debug` lors du développement.

### 2. Modifiez le nom du système

Dans le panneau d'administration, allez dans `Settings > General Settings > System Name` et remplacez Overdog par le nom du client.
Ce terme est visible seulement dans le panneau d'administration et lors de la connexion.
Si le nom du client est long, utilisez une version courte. :upside_down_face:

### 3. Modifiez le nom du groupe de sites

Craft CMS regroupe les langues selon des `sites`. Chaque site représente donc une langue dans 95% des cas. Vous allez renommer le groupe qui regroupe ces langues.
Allez dans `Settings > Sites` Cliquez sur Overdog puis sur la roulette de configuration et renommer selon le nom du client.

::: tip
Le nom de ce groupe sera le nom du site utilisé dans le `head` et dans vos _templates_.
Assurez-vous que c'est le bon nom du client.
:::

### 4. Ajoutez une langue (si nécessaire)

Au même endroit : `Settings > Sites`.

Pour ajouter la langue anglaise à un site, cliquez sur le bouton __NEW SITE__.
1. Inscrire English à `Name`
2. Le Handle doit être `siteEnglish`
3. Utilisez `en-CA` si le site est au Canada
4. S'assurer que __This site has its own base URL__ est coché.

::: tip
Mettre dans __Base URL__ la variable de votre fichier .env correspondant à cette langue. Exemple, pour une langue supplémentaire en anglais : __$SITE_URL_EN__
:::

## Fichier .env > LastPass

### Ajouter le contenu de votre fichier .env dans _LastPass_

1. Dans _LastPass_, faire __Add a secure note__
2. Nommez votre note : __[ENV] Nom du client__
3. Mettre la note dans le dossier du client. Exemple : `Shared-Clients/Nom-du-client`
4. Copiez l'entièreté du fichier `.env` en enlevant les paramètres uniques à votre ordinateur (db, url, etc.).

#### Exemple __sans__ les renseignements. Les variables peuvent varier selon le projet.

``` sh
SITE_URL=
ENVIRONMENT=dev
CRITICALCSS=false
SECURITY_KEY= ****************
APP_ID=****************
DB_DATABASE=
DB_DRIVER=mysql
DB_PASSWORD=
DB_PORT=
DB_SERVER=
DB_USER=

S3_BUCKET_DOCS=********
S3_BUCKET_IMAGES=********
S3_BUCKET_REGION=ca-central-1
S3_BUCKET_SUBFOLDER=********
S3_DOCS_URL=********
S3_DOCS_DIST_ID=********
S3_IMAGES_URL=********
S3_USER_ID=********
S3_USER_SECRET=********
```

::: danger
Votre fichier `.env` ne doit **jamais être dans un _commit_.** Par défaut, il sera ignoré dans le fichier `.gitignore`.
__Ne pas changer ce comportement__.
:::


## Gestion des assets

### Volumes - Craft CMS <Badge text="v1.1+"/>

Les volumes sont utilisés pour stocker les documents et images du client. Nous priorisons le stockage via _Amazon Web Services_ avec le [plugin first-party Amazon S3](https://plugins.craftcms.com/aws-s3).

::: warning
Les volumes sont créés par défaut avec Overdog. Vous avez seulement à bien remplir les variables du fichier `.env` avec les informations nécessaires.
:::

[Voir les variables .env des volumes](/installation/nouveau-projet/#informations-sur-les-volumes-de-stockage)

#### Voici les volumes créés par défaut :

| Name | Handle  |
| ------ | -----|
| Documents | volumeDocuments |
| Images | volumeImages |

::: tip
Dans 95% des projets, ces volumes pré-configurés répondront à vos besoins. Vous pouvez par la suite faire des sous-dossiers avec les _fields_. Demandez à un responsable avant d'ajouter un volume.
:::


### Dossier client - AWS <Badge text="v1.1+"/>

Demandez à un responsable de créer l'utilisateur _programmatic access_ dans `IAM` pour le CMS du client :punch:

- Il doit faire partie du groupe `tjclients-cms`
- __Le nom d'utilisateur sera celui de son dossier client (aucun dossier S3 à créer)__
- Ce nom doit être _url-friendly_, il se retrouvera dans le _Url_ du CDN, autant pour les documents que les images. Exemple de nom : maison-merry, valleyfield, gymini, etc.
- Le CMS du client aura accès __seulement à ce dossier__
- Le nom de l'utilisateur (et du même coup de son dossier) sera la valeur de la variable `S3_BUCKET_SUBFOLDER` du fichier `.env`
- Vous aurez aussi besoin du `key id` et du `key secret` de l'utilisateur créé



### Fields Assets - Craft CMS

Vous pouvez créer des sous-dossiers lors de la création d'un `field` dans le champ _Default Upload Location_ ou directement dans le menu latéral `Assets`.

Exemple: `/news`, `/documentation`, etc.

::: tip
Si vous ajouter le sous-dossier lors de la création d'un field (_Default Upload Location)_, mettez un ` / ` devant le terme désiré.
Vous pouvez consulter le _field_ `seoOgImage` pour un exemple avec sous-dossier.
:::

## Rôles et groupes d'utilisateurs

### Groupes d'utilisateurs client

Créez les groupes d'utilisateurs du client __APRÈS__ la création de votre structure (sections, etc.), __idéalement vers la fin de votre projet__ car les accès sont granulaires. Vous définissez pour chaque section les accès.

1. Priorisez la création de groupes de permissions. Ces groupes seront dans la structure du projet, donc _commités_ et stockés dans les fichiers de config `yaml`.
2. Créez par exemple le groupe `userGroupsWebmasters` avec les permissions désirées ou des groupes plus scindés si c'est un projet d'envergure avec plusieurs types de permissions.

::: danger
- Les __GROUPES d'utilisateurs__ doivent être __créés en local__.
- Les __UTILISATEURS__ doivent être créés sur le __site live__ (base de données principale) en les associant à ce groupe.
:::

### Nomenclature des utilisateurs

_Plus d'information à venir_

### Utilisateurs 3e joueur

#### Admin par défaut

- Le compte principal doit se nommer `3ejoueur`.
- Utilisez un gestionnaire de mot de passe ou un outil pour générer un mot de passe solide.

#### Comptes supplémentaires

- Si des utilisateurs de 3e joueur doivent s'ajouter au projet. Exemple : Anne-Marie et Jean-Philippe.
- Créez des utilisateurs de cette façon : `3ejoueur-annemarie` et `3ejoueur-jp`.
- Cela permettra de les supprimer à la fin du projet et de transférer le contenu créé au compte 3ejoueur.
