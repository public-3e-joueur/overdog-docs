---
title: Déploiement
lang: fr-CA
---

# Déploiement

Contenu à venir

Il sera question de :

- Faire un build avant de déployer
- Déploiement par Git chez Fortrabbit



## Nouveau projet chez Fortrabbit


### Valeurs du fichier .env

Allez mettre vos informations de fichier env dans le dashboard Fortrabbit de votre app.
Inscrivez seulement les valeurs manquantes. Fortrabbit gère celles de la base de donnée.
N'oubliez pas la valeur `SITE_URL` ;) Been there, done that!


### Connectez-vous à la base de données distante via SSH. Détails à venir.

Faites un dump de votre base de données locale et importez-la dans celle live.



### Déploiement des fichiers avec Git


1. Aller dans le dossier de votre projet avec le terminal et ajouter fortrabbit comme remote

::: tip
Vous trouverez cette info dans l'onglet Git / Clone url dans le dashboard Fortrabbit de votre app.
:::

#### Pour le live :

``` sh
git remote add frb-live {{ Clone URL }}
```

#### Pour un staging :

``` sh
git remote add frb-staging {{ Clone URL }}
```

::: danger
Exemple seulement, vous devez aller dans le __dashboard Fortrabbit__ chercher l'information du Git de votre app.
:::

Fortrabbit sera ajouté dans votre projet comme remote et visible dans votre logiciel SourceTree, Tower, etc.


2. Faire ensuite la commande suivante en remplaçant `local branch name` par le nom de votre branche locale :

#### Pour le live :

``` sh
git push frb-live <local branch name>:master
```

#### Pour un staging :

``` sh
git push frb-staging <local branch name>:master
```

Le premier commit est long, vous verrez toutes les étapes dans votre terminal.

::: tip
Attendez quelques secondes et allez voir votre site en ligne. Si un problème, ne paniquez pas.
Il manque probablement une variable à votre fichier env ou le script composer post-install n'a pas terminé chez Fortrabbit.
:::


## Projet existant

### Déploiement chez Fortrabbit

1. Faire l'étape 1 de Nouveau projet pour ajouter Fortrabbit comme remote.
2. Faire un fetch sur le remote pour tirer la branche Master.
3. Déployer sur master via votre logiciel ou le terminal.


## Reset de repo chez Fortrabbit

Dans les forfaits utilisés chez Fortrabbit par 3e joueur, nous n'avons pas l'option des déploiements atomiques (forfaits standards). C'est donc dire que les déploiements par `Git` remplacent les fichiers existants, mais ne suppriment pas.

__Cela peut être problèmatique dans le cas des fichiers de `project config` de Craft.__

### Mise en situation

Imaginez supprimer un _field_ en local (le fichier `YAML` de _project config_ est ainsi supprimé) et vous déployez ensuite. Sur le _live_, chez Fortrabbit, l'ancien fichier `YAML` sera encore présent (le _commit_ ne supprimant pas). Donc, dans le panneau de contrôle, vous aurez une mention "_project config out of sync_".

::: warning
Si cela vous arrive, __ne cliquez pas__ sur la bande rouge qui demande de _"review project config"_. 
:::

### Le RESET du repo à la rescousse

Un _reset_ du repo chez Fortrabbit va supprimer le contenu du serveur (l'historique des commits) et partir un nouvel historique, celui que vous allez déployer.

1. Simplement faire dans votre terminal, faire `ssh {votre projet}@deploy.us1.frbit.com reset`

2. Vous pouvez _fetch_ votre remote `frb-live` pour voir que l'historique des commits a disparu.

3. Déployer votre projet via le terminal :

``` sh
git push frb-live <local branch name>:master
```

### Non-destructive

Le reset est une opération _non-destructive_. Votre projet va continuer de fonctionner normalement sans interruption. 

### À noter

Lors de déploiement de gros sites, il peut y avoir un délai allant jusqu'à 4-5 minutes avant que le panneau de contrôle d’admin enlève la mention ( _review project config_ ). 