---
title: Nouveau projet
lang: fr-CA
meta:
  - author: Ian Reid Langevin
---

# Templating

## Traductions

### Fichiers de traduction

Dans vos fichiers de _templates_, __utilisez l'anglais pour vos strings__. Pour les traduire, vous devez les copiez dans le fichier `site.php`, dans le dossier nommé selon le code de la langue désirée.

Exemple pour la langue française : `translations > fr > site.php`

``` php
<?php

return [
  /* Menu */
  "To do" => "À faire",
  "Our addresses" => "Nos adresses",
  "Discover" => "Découvrir",
  /* Info block */
  "Contact" => "Coordonnées",
  "Location" => "Lieu",
  "Informations" => "Informations",
];
```

::: tip
Vous pouvez même traduire vos formats de date ! Par exemple : `"F j, Y" => "j F Y"`
:::

### Utilisation dans votre template

Dans votre template, ajoutez le filtre `|t` à votre _string_

``` php
{{ 'All day'|t }}
```

[Documentation officielle > traductions](https://craftcms.com/docs/3.x/sites.html#static-message-translations)



## Alias personnalisés

Overdog inclut des alias personnalisés que vous pouvez utiliser dans vos _templates_.

- `@siteUrl`
@siteUrl est à utiliser lorsque que vous souhaitez référer au URL de base de votre site __INCLUANT LA LANGUE__.
Exemple : monsite.com/en sera le _siteUrl_ de base en langue anglaise.

- `@rootUrl`
@rootUrl est à utiliser lorsque que vous souhaitez référer au URL de base de votre site __SANS LA LANGUE__.


::: danger
__Ne jamais utiliser__ l'alias @web dans nos templates pour éviter une _cache poisoning vulnerability_.
:::


#### Exemple minimaliste de lien vers la page d'accueil

``` html
<a href="{{ alias('@siteUrl') }}">Texte du lien</a>
```

#### Exemple de lien vers une page et traduction des _strings_

``` html
<a href="{{ alias('@siteUrl') ~ 'events'|t }}" target="_self" rel="noreferrer">{{ 'All events'|t  }}</a>
```
Note : il s'agit d'un exemple si le _slug_ events ne change pas avec utilisation d'un _alias_. Vous pourriez autrement utiliser quelque chose comme :

``` html
<a href="{{ craft.entries.id(14).one().getUrl()|default() }}">{{ 'All events'|t  }}</a>
```

[Documentation officielle > alias](https://craftcms.com/docs/3.x/config/#aliases)

## Cache

Pour des éléments dans la navigation ou dans le bas de page qui sont utilisés partout, sur tout le site, utilisez :
`{% cache globally %}`

Pour ce qui est unique à une page, utilisez :
`{% cache %}`

::: danger
__Toujours toujours mettre une `key` à la balise `{% cache %}`.__
:::

#### Exemple avec une _key_

``` twig
{% cache using key "page-header" %}
```

#### Vous pouvez ajouter une durée et des paramètres, exemple :

``` twig
{% cache globally for 1 year using key "navigation" %}
```

[Documentation officielle > cache](https://craftcms.com/docs/3.x/dev/tags.html#parameters)

## SEO

### Groupe de _fields_ par défaut

Overdog crée deux _fields_ par défaut lors de l'installation.

`seoDescription` et `seoOgImage`

Ces champs se trouvent déjà un global pour les valeurs par défaut et de la page d'accueil du site.
Par contre, vous devez les ajouter sur les types d'entrées que vous désirez.

::: tip
Dans le `<head>` de votre template `base > layout.twig`, le contenu sera automatiquement utilisé si ces champs sont remplis pour une entrée.
:::

### Bloc JSON-LD

Au bas de votre fichier `base > layout.twig`, vous retrouvez le bloc suivant :

``` twig
{# SCHEMA.ORG INFO #}
{% block schema %}{% endblock %}
```

### Exemple pour une entrée Recipe

Si pour un type d'entrée donné, vous souhaitez ajouter de l'information _Schema / JSON-LD_, simplement ajouter le bloc suivant et modifier les valeurs selon les champs désirés.

Ce bloc doit se retrouver __dans le template de cette entrée__ (dans le haut par exemple).

``` twig
{% block schema %}
  <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Recipe",
      "name": "{{ entry.title }}", {# twig directement dans le js #}
      "author": {
        "@type": "Person",
        "name": "{{ entry.schemaAuthor }}" {# twig directement dans le js #}
      },
      "datePublished": "2018-03-10",
      "description": "{{ entry.schemaDescription }}", {# twig directement dans le js #}
      "prepTime": "PT20M"
    }
  </script>
{% endblock %}
```

[Documentation provenant de Google](https://developers.google.com/search/docs/guides/intro-structured-data)


## Bonnes pratiques

### Ignore missing dans les _include_

Il est considéré une bonne pratique d'ajouter `ignore missing` lorsqu'on utilise un `include`.
Cela permet à Twig de l'ignorer si le _template_ est manquant plutôt que d'afficher une erreur.

``` twig
{{ include('_shared/textblock', ignore_missing = true) }}
{# exemple avec une variable qui est passée #}
{{ include('_shared/textblock', {iconName: 'date'}, ignore_missing = true) }}
```
