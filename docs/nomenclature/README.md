---
title: Configuration
lang: fr-CA
---

# Structure des dossiers et nomenclature<Badge text="v1.1+"/>

Voici la nomenclature pour chaque type de contenu dans le panneau d'administration.

::: danger
Utilisez toujours les __camelCases__ pour être cohérent avec la façon native de Craft CMS
:::

## Sections

Nommez les `handles` de vos sections selon la formule suivante `[section type][name en anglais]`.
Le nom des structures et channel est visible pour le client, celui-ci peut être en français.

#### Exemples si le client est francophone :

| Type de section        | Name           | Handle  |
| ------------- |:-------------:| -----:|
| Channel     | Nouvelles | channelNews |
| Structure      | Équipe |   structureTeam |
| Single      | Accueil - Single |   singleHome |


#### Exemples si le client est anglophone :

| Type de section        | Name           | Handle  |
| ------------- |:-------------:| -----:|
| Channel     | News | channelNews |
| Structure      | Team      |   structureTeam |
| Single      | Home - Single |   singleHome |

::: tip
__Les handle seront donc toujours uniques. Que faire si j'ai 2 noms identiques ?__

Comme le nom de section doit être unique dans le panneau, si par exemple, vous avez un Single et un Channel avec le même nom, nommez le Single `Nom du single - Single`. Donc, toujours utiliser le nom "court" pour les structures et channel car un single sera renommé par son champ Title.
:::


## Entry types multiples

#### Exemples si une section a plusieurs entry types :

Essayez de conserver un nom de handle simple et court pour les entry types.

| Section | Entry types name | Handle  |
| ------------- |:-------------:| -----:|
| structureTeam | Boss | boss |
|                   | Employee |   employee |
|                   | Council |   council |
| - | - | - |
| structurePages | Mission |   mission |
|                   | Événements |   events |

::: tip
__Pourquoi le entry type possède un nom simple ?__
1. C'est scopé à la section
2. Dans le dossier de templates, le dossier sera nommé selon ce _handle_ et le `index.twig` de la structure va pointer automatiquement vers ce dossier.
:::

## Fields

### Groupes de fields dans la sidebar

Regroupez vos _fields_ par groupes en utilisant le nom de vos sections.
Voici quelques exemple :

• structureTeam, structurePartners, channelNews, etc.

Si des _fields_ sont partagés, un groupe devra être créé dans la sidebar et nommé _shared_.
Pour les fields globaux, un groupe devra être créé et nommé _global_.

### Nomenclature - Fields d'une section avec _1 seul entry type_

Préfixer les fields du nom de la section. Exemple pour un channel _channelEvents_ avec 1 seul entry type :

| Name           | Handle  |
| ------------- |-------------|
| Lieu | <b>channelEvents</b>Location |
| Date de début  | <b>channelEvents</b>StartDate |


### Nomenclature - Fields d'une section avec _plusieurs entry types_

Si votre section a __plusieurs entry types__, le entry type se retrouvera dans le _handle_. Exemple pour une section `structurePages` avec plusieurs entry types.

Dans la sidebar, un seul field group sera présent et nommé `structurePages`.

| Name           | Handle  |
| ------------- |-------------|
| Présentation | <b>structurePagesAbout</b>Presentation |
| CTA équipe  | <b>structurePagesTeam</b>CtaText |
| Valeurs  | <b>structurePagesAbout</b>Values |

::: tip
__Pourquoi préfixer avec la section ?__
Imaginez un gros site avec une structure Events, mais également un single Events (disons une page).
Les deux sections possèdent un field Intro, mais différent.
Le préfixe permet de régler cela facilement.
Nous aurions dans ce cas, un _field_ `singleEventsIntro` et un _field_ `structureEventsIntro`.
:::


## Matrix fields

Le matrix field va prendre le handle tel que vu précémment. Pour une matrix principale partagée à travers plusieurs sections, ce serait exemple : `sharedMainMatrix`.

#### Les blocks

Les blocs d'une matrix sont scopés à cette matrix. Utilisez seulement le préfix __block__.

`blockTextImg`, `blockGallery`, etc.

#### Les fields des blocks

Pour les fields de vos blocs, utilisez ensuite le nom du bloc comme préfix. Exemple :

`blockGalleryIntro`, `blockGalleryAssets`, `blockGalleryLink`, etc.

## Category Groups

Pour les groupes de catégories, utilisez le _Handle_ sous la forme suivante : categoryNameOfMyCategory

__Quelques exemples :__

| Name           | Handle  |
| ------------- |-------------|
| Activités Type - Catégories | categoryActivitiesTypes |
| Nouvelles Sujet - Catégories | categoryNewsSubject |
| Fruits couleurs - Catégories | categoryFruitsColors |

## Global fields

À venir
