---
title: Changelog
lang: fr-CA
---

# Changelog
Tous les changements notables sont documentés dans cette section.
Overdog adhère au [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
- Add a custom 503 error page
- Major : Add lint-staged for commit staged file lint
- Add a way to manage Content Security Policy (CSP) - [Mozilla](https://developer.mozilla.org/fr/docs/Web/HTTP/CSP)

## 1.4.0
### 2022-01-26
#### Added - Major changes
- System Name, Site group and System Status are now in .env variables.
- New .vscode folder if you want to use same settings than linters
- Update all the base SCSS structure to use the new @use and @forward (@import is deprecated)
- Add SailorCSS (^2.0) config in the SCSS folder, remove it if you do not use SailorCSS
- Add SailorCSS as default dependency in package.json

#### Added - Minor changes
- Update _Can I use_ browsers list
- Update Craft CMS to 3.7.30.1 and Redactor plugin to 2.8.8


## 1.3.6 
### 2021-12-13
#### Added
- Composer post-install script clear Craft CMS compiled-templates and not all caches anymore
- New selector-class-pattern in stylelint config
- Increase indent rules in eslint
- Convert the site locale to og format in head (replace hyphen by underscore)

## 1.3.5
### 2021-11-10
#### Added
- Added a macro to easily format phone number
- Changed `getenv` to `App::env` in `config/db.php` and `config/general.php` -> Craft CMS new best practice
- SCSS - `mixins.scss` and `placeholders.scss` are now two single files in the abstracts folder
- Changed font-family var to match SailorCSS syntax (not an issue if you do not use SailorCSS)
- Changed the `project-config/apply` and `migrate/all` scripts in post-install-cmd to the new `craft up/index` command
- Renamed JavaScript a11y.js file and add subfolder
- Updated Craft CMS to 3.7.20
- Updated Redactor plugin and Composer dependencies
- Updated comments on the SeoOgImage field in CP

#### Fixed
- Fixed French apostrophe character in translations
- Fixed keypress depreciation in `focus.js`
- Fixed old comment for critical css in `layout.twig`

#### Removed
- Removed opinionated margin and color on small, figcaption in SCSS file root.scss


## 1.3.4
### 2021-09-22
#### Added
- Added Craft CMS sameSiteCookieValue setting in `config/general.php`
- Updated Craft CMS to 3.7.10
- Added comment in `config/route.php`
- Added the feature to show a language switcher if a page is not an entry. Info in the `language-switcher.twig` comment


## 1.3.3 
### 2021-08-09
#### Added
- Update Craft CMS to 3.7.8


## 1.3.2
### 2021-08-09
#### Added
- Added some classes to the Purge CSS safelist in Webpack prod config
- Added revAssetUrls Craft new setting in config


## 1.3.1
### 2021-07-09
#### Added
- Imgix Size and crop for og:image


## 1.3.0
### 2021-07-05
#### Added
- No more yarn svg command, svg added in the svg/sprite-images are automatically optimized and added to the sprite.
- The @svgPath in config is no longer needed. Use svg only with the id. Ex : `<svg><use xlink:href="#logo-3ejoueur"></use></svg>`
- SVG are now injected with JS after page load
- Hash in file name for the svg.js file for cache busting on svg changes
- Updated Craft CMS to 3.6.17
- Added 'backupOnUpdate' => false in Craft config. Manually do it on your host server for better performance.
- Changed the include statement in layout.twig for include function - [Twig note](https://twig.symfony.com/doc/3.x/tags/include.html)

#### Removed
- Important > Remove opinionated SCSS files. We kept only the folder structure and the reset / base. You can now use any framework with Overdog easily.
- Remove some class in 404 file


## 1.2.9
### 2021-06-28
#### Added
- Added the 'maxRevisions' => 6 to the Craft Config file.
- Added the ignore blockless-at-rules rule in max-nesting-depth Stylelint config
- Updated yarn dependencies

#### Fixed
- Fixed border none to border 0 in reset
- Fixed typo in webpack Config




## 1.2.8
### 2021-06-10
#### Removed
- Removed Lazyload JS and Scss - download it on [navig.dev](https://navig.dev)



## 1.2.7
### 2021-05-17
#### Added
- Added Permission-policy opt out google FLoC in htaccess
- Splitted the Webpack runtime (manifest) in a JS chunk - see docs for details
- Updated webpack and add new output clean config
- Modified the redactor minimalist config to avoid pasting style, thanks to Melissa Doyon
- Added settings in Webpack config for dynamic imported chunks

#### Removed
- Removed Webpack CleanWebpackPlugin - useless with the new clean setting
- Removed Flickity from project dependencies - no more assumptions for front-end components



## 1.2.6
### 2021-04-14
#### Fixed
- Fix return 0 on post-install-cmd for exit in composer.json


## 1.2.5
### 2021-04-06
#### Fixed
- SCSS > Fix a footer error on build


## 1.2.4
### 2021-04-06
#### Added
- Fuzzy search is now by default in general.php config file
- Better consistency in breakpoints based on Bootstrap 5 for the srcset generator (modify for your own if needed)
- SCSS > add display block to the time tag in reset
- Update Craft to 3.6.11.2

#### Removed
- SCSS > Remove color and border to fieldset in reset

#### Fixed
- SCSS > Fix footer SVG path property


## 1.2.3
### 2021-03-10
#### Added
- Changed the lazyload.js function to target all the `loading="lazy"` attribute instead of the `.lazyload` class
- Note : The `lazyload` class is no longer needed on image or `iFrame` for lazyloading
- Added dynamic import syntax plugin for Babel to allow dynamically load modules in JS through Webpack config - [see Webpack docs](https://webpack.js.org/api/module-methods/#import-1).
- Update to Craft CMS 3.6.10

#### Removed
- Removed the lazy-wrapper class. It's replace by new `lazy-fit` and `lazy-img` classes.
- [See details and the new doc](/utilitaires/#lazyload).
- Remove the default navbar and the menu - Use [navig.dev](https://www.navig.dev) to add some components.

## 1.2.2
### 2021-02-28
#### Fixed
- Missing the terser plugin in new Webpack 5 config to minimize JS

## 1.2.1
### 2021-02-05
#### Fixed
- Updated Craft CMS to 3.6.4 for some bug fixes

## 1.2
### 2021-02-04
#### Added - Major changes
- Updated Craft CMS to 3.6.x
- Updated Webpack 4 config to Webpack 5 [details](https://webpack.js.org/migrate/5/)
- Review webpack config and clean up some dev dependencies

#### Added - Minor changes
- Updated browserlist can i use dependency - fix a warning on build

#### Fixed
- Removed the depreciated siteUrl config in Craft CMS in general.php
- Environnement variables are now used for each language (see env.example)

#### Removed
- Removed some project dependencies in package.json (Bootstrap, jquery, Flickity)
( no more assumptions, add packages if you need them )

## 1.1.5
### 2021-01-20
#### Added
- Updated Redactor plugins to 2.8.5 and some dependencies
- Added a foreach loop for css files generated by HtmlWebpackPlugin in the EJS template
  ( useful if a node dependency is splitted in chunks for css )

#### Fixed
- Renamed home single folder to fit with the documentation naming convention
- Sitemap home handle was the old one before v1.1
- Replaced some double quotes by single in Twig templates for consistency

#### Removed
- Removed typography mixins - was useful with Font Face Observer for opacity transitionning
  ( we are now using the native display:swap or display:optionnal browsers feature - now supported by Adobe font too )

## 1.1.4
### 2020-12-21
#### Added
- Replaced Lazysizes.js with our own lazyload function to use the browser native if supported (based on Google web.dev)
- Changed default volumes names for consistency
- Renamed macro default file to index for consistency
- Post-install-cmd command in composer.json if Craft is installed - Run after deployment
- Changed Webpack Sass loader to Dart Sass
- Add CSS longer cache policy in htaccess

#### Fixed
- Double rel tag in footer copyright link
- Wrong path for the 404
- Fix url on logo if multilanguage (was always the first language)

#### Removed
- Removed SVG sprite pre-fetch in head (was duplicate on page load with pre-fetch - investigation to come)



## 1.1.3
### 2020-11-20
#### Added
- Changed Webpack Sass loader to Dart Sass
- Updated Craft to 3.15.1
- Added new Scss lint order by properties
- Added the site language in OG:current and OG:alternate auto dynamically

#### Fixed
- Breakpoint in body font size (was xxl, supposed to be lg to match root)



## 1.1.2
### 2020-11-04

#### Added
- Added ESlint for JS with standardjs.com config
- Changed the order of SCSS lint to rational order
- New commands for linting for consistency
- Split the scss reset file and change root value to percentage for a11y

#### Fixed
- Changed the siteName global variable in navbar for currentSite.group.name
- Remove the picture tag in the lazy utility class lazy-wrapper, added in the doc if needed

#### Removed
- Unused testing dependencies in package.json




## 1.1.1
### 2020-10-28


#### Added - Minor changes
- Added bucket folder variable in the cloudfront prefix in document volume
- Added some opinionated mentions and credits in utilities > scss

#### Fixed
- Fixed the new url for documentation and changelog in the README.example and base README
- Uniformisation of JS parts





## 1.1
### 2020-10-26

#### Added - Major changes
- S3 plugin for volumes is now default
- 2 volumes are already created with the .env variables
- Added volumes values for S3 in the env.example
- Added a CriticalCSS boolean variable in the .env file to inline it or not in the head. It's now separate from the environment

#### Added - Minor changes
- Added a `README.md` for client projets with the documentation link and version number
- Added `ignore missing` on include tag in layout
- Added seoOgImage field a default subfolder `/seo`
- Added two alias for images and documents cdn for head preconnect (`condig > general.php` and `templates > base > layout.twig`)
- Renamed : home handle for homeSingle and volumes handles to match the documentation
- Updated to Craft 3.5.14
- Added function to detect is user press tab and then, add outline styles (`src > js > parts > a11y.js`)

#### Removed
- Removed Fortrabbit craft-auto-migrate plugin
- Removed alias for local assets
- Removed fallback to @web in config for siteFrench alias

#### Fixed
- Removed freeform plugin from composer.lock
- Removed `--inline` in the package.json at the end of the `SVG` script. (issue if used without svg4everybody polyfill)





## 1.0.1
### 2020-10-14

#### Added
- Added Responsive font size - RFS for spacing utility. New classes created from one value.
- Changed the folder for home page to match new folder structure.
- New documentation

#### Removed
- Removed unused link-decoration and base-letter-spacing variables.




## 1.0.0
### 2020-10-06
- Initial stable release
