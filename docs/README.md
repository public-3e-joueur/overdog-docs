---
title: Overdog Documentation
lang: fr-CA
meta:
  - author: Ian Reid Langevin
  - description: Base de projet pour Craft CMS intégrant plusieurs technologies modernes et bonnes pratiques.
---

# Overdog Documentation

## Wassup joueur étoile!

Overdog est une base de projet pour Craft CMS intégrant plusieurs technologies modernes et bonnes pratiques. Retrouvez ici toute la documentation pour débuter.
Utilisez le menu latéral pour naviguer entre les sections.

### Documentation officielle

#### Craft CMS

- [Documentation officielle - Craft CMS :rocket:](https://craftcms.com/docs/)
- [Documentation officielle - Extending Craft > plugins et modules](https://craftcms.com/docs/3.x/extend/)
- [Documentation officielle - API > Craft 3 Class Reference](https://docs.craftcms.com/api/v3/)
- [Channel Discord officiel](https://craftcms.com/discord)

#### Craft Commerce

- [Documentation officielle - Craft Commerce](https://craftcms.com/docs/commerce/3.x/)
- [Documentation officielle - Extending Commerce](https://craftcms.com/docs/commerce/3.x/extend/)
- [Documentation officielle - API > Commerce Class Reference](https://docs.craftcms.com/commerce/api/v3/)

### Ressources

#### Articles utiles

- [Articles rédigés par Andrew Welch de nystudio107](https://nystudio107.com/blog)
- [La section New to Craft de craftquest.io](https://craftquest.io/quests/new-to-craft)

#### Hébergement, déploiement et
- [La documentation de Fortrabbit](https://help.fortrabbit.com/)
- [Blog de Fortrabbit](https://blog.fortrabbit.com)

### Auteur et contributeurs

- Ian Reid Langevin (auteur)
- Mélissa Doyon (contributrice)
- Sylvain Larek (contributeur)

[3ejoueur.com](https://3ejoueur.com)
