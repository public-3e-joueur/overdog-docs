# Utilitaires

## Optimisation SVG <Badge text="v1.3+"/>

Overdog permet d'optimiser et de nettoyer les SVG __automatiquement__ et de créer un sprite avec ceux-ci.

### Fonctionnement
Placez vos fichiers SVG dans le dossier `src/svg/sprite-images`. Ces fichiers seront ensuite combinés en _symbol_ dans un fichier JavaScript qui les injectera dans votre DOM __après le chargement de la page__.

### Utilisation du sprite dans vos templates

Pour utiliser un SVG du _sprite_ ainsi créé, utilisez la façon suivante. Remplacez `#logo-overdog` de l'exemple par le nom original du SVG désiré. Le nom de chaque fichier SVG est utilisé pour créer son ID dans le _sprite_.

``` html
<svg class="votre-classe"><use xlink:href="#logo-overdog"></use></svg>
```

Le `use xlink:href` va aller chercher le code du _symbol_ `#logo-overdog` dans le _sprite_ et l'afficher à l'intérieur de la balise SVG.
Le `xlink` est déprécié au profit de seulement `href`, mais Safari 11(2017) en a besoin. Il est encore suggéré de le conserver.

::: tip
Si votre page ne contient pas de `use xlink:href`, le fichier JS des SVG ne sera pas chargé.
:::

### Anciennes versions de Overdog (avant v1.3)

Si vous utilisez une version de Overdog __avant v.1.3__, placez vos fichiers SVG dans le dossier `src/svg/sprite-images`. Faites à la racine de votre projet dans le terminal : `yarn svg`.
Vous devrez ajouter l'alias `@svgPath` avant votre ID de SVG dans votre HTML. Regardez les exemples déjà faits dans votre projet.

### Modifier la couleur via votre feuille de style SCSS / CSS
Comme le _SVG Sprite_ est dans un shadow DOM, vous devez supprimer les balises `fill=` de votre fichier SVG si vous souhaitez modifier sa couleur via votre SCSS par la suite (aux groupes ET aux paths).
Vous pouvez aussi ajouter une variable CSS dans le code du SVG. Exemple `fill=var(--primary)`.

### Utilisation _scalable_ (sans _width_ ou _height_)

Pour redimentionner un SVG responsivement, c'est assez complexe. Si votre div a un `height` et un `width` bien définis, c'est super _(ex: icônes, illustrations, etc.)_.
Mais si vous souhaitez qu'un SVG utilise tout l'espace disponible en largeur, sans avoir de dimension (ex: divider). Disons _fit the available width_, c'est plus tricky.

::: tip
__Un truc__ : Prenez le viewbox du symbol et mettez le sur votre `<svg>` tag dans votre template. Mettez ensuite le width ou le height à `auto`.
Les navigateurs vont ajuster automatiquement en respectant le ratio du viewbox.

[Article de Amelia Bellamy-Royds sur Css-tricks](https://css-tricks.com/scale-svg/).
:::

## Linting

### Commandes

Pour _linter_ et _autofixer_ le Scss et Javascript de votre projet,
allez à la racine du projet avec votre terminal.

#### SCSS

La _config_ utilisée est basée sur le SCSS guidelines


``` sh
yarn lint:scss
```

#### JS

La _config_ utilisée est celle de [standardjs.com](https://github.com/standard/eslint-config-standard)

``` sh
yarn lint:js
```


#### Pre-commit hook

Fonctionnalité à venir



## Lazyload

### Fichiers nécessaires

1. Ajoutez les fichiers `lazyload.js` et `_lazyload.scss` de [navig.dev](https://www.navig.dev/lazyload) dans vos dossiers JS et SCSS de projet et importez-les (probablement dans app.js et index.scss).

### 1. Utilisation avec un format défini par vous, selon vos dimensions (pour un effet similaire à background-image)

- Un `div` avec la __classe `lazy-fit`__ doit entourer votre tag `img` pour l'arrière-plan.
- Ajoutez une classe pour votre style custom (dimensions, etc.).
- Ce div doit avoir un `width` et un `height` ou un `padding-bottom` pour un ratio responsive.

``` twig
{% set photo = entry.yourAssetsField.one() %}
{% if photo %}
  <!-- div wrapper avec lazy-fit -->
  <div class="lazy-fit votre-classe-de-style">
    <!--
    auto=format permet à Imgix de retourner un WEBP ou un JPG
    Vous pouvez aussi utiliser auto=compress,format pour une compression agressive
    Les sizes sont en exemples, ajustez selon vos changements de colonnes
    -->
    <img
      loading="lazy"
      decoding="async"
      data-srcset="{{ photo.url }}&w=1500&auto=format 1500w,
                   {{ photo.url }}&w=1280&auto=format 1280w,
                   {{ photo.url }}&w=600&auto=format 600w"
      sizes="(min-width: 1200px) 40vw, (min-width: 768px) 60vw, 100vw"
      <!-- src is only for scrset non-support browser -->
      data-src="{{ photo.url }}&w=900"
      alt="{{ photo.title }}"
     />
  </div>
{% endif %}
```


### 2. Utilisation avec un format __d'image inconnu (exemple: provenant du CMS)__

- Un `div` avec la __classe `lazy-img`__ doit entourer votre tag `img`.
- __Ajoutez les attributes height et width__ à la balise `<img>`. [En savoir plus via Malte Uble de Google](https://www.industrialempathy.com/posts/image-optimizations/#responsive-layout)
- Le div qui _wrap_ doit avoir un width défini.

``` twig
{% set photo = entry.yourAssetsField.one() %}
{% if photo %}
  <!-- div wrapper avec lazy-img -->
  <div class="lazy-img votre-classe-de-style">
  <!--
  auto=format permet à Imgix de retourner un WEBP ou un JPG
  Vous pouvez aussi utiliser auto=compress,format pour une compression agressive
  Les sizes sont en exemples, ajustez selon vos changement de colonnes
  -->
    <img
    height="{{ photo.height }}"
    width="{{ photo.width }}"
    loading="lazy"
    decoding="async"
    data-srcset="{{ photo.url }}&w=1500&auto=format 1500w,
                 {{ photo.url }}&w=1280&auto=format 1280w,
                 {{ photo.url }}&w=600&auto=format 600w"
    sizes="(min-width: 1200px) 40vw, (min-width: 768px) 60vw, 100vw"
    <!-- src is only for scrset non-support browser -->
    data-src="{{ photo.url }}&w=900"
    alt="{{ photo.title }}"
   />
</div>
{% endif %}
```

### Comportement

::: warning
Avec les exemples suivants, on veut éviter un _layout shift_ de la page lorsque les images sont chargées.
__Un arrière-plan gris léger remplace donc l'image jusqu'au défilement et chargement.__
Celle-ci sera affichée avec un effet d'opacité.
:::

## Webfonts

Priorisez la fonctionnalité native des navigateurs concernant le chargement des polices.

Utilisez dans vos `@font-face` :
``` css
font-display: swap;
```

::: tip
 Si vous utilisez un kit web de typographies web, n'oubliez pas d'aller cocher dans votre Adobe kit ou Google webfont kit cocher ou activer le `font-display: swap`.
:::

## PurgeCSS <Badge text="v1.0.1+"/>

__PurgeCSS__ efface automatiquement toutes les classes inutilisées dans vos `templates` (dossier templates) et les supprime lors du _build_.

Notez que le `reset.scss` et `root.scss` sont _whitelistés_. Vous pouvez ajoutez des classes whitelistées dans le fichier `webpack.prod.js`.

::: warning
Dans votre fichier `index.scss`, vous retrouverez les mentions `/*! purgecss start ignore */` qui entoure des fichiers _whitelistés_ en entier.
__À manipuler avec précaution!__
:::

## Critical CSS

Généré automatiquement lors du build pour la page d'accueil. Info à venir.
